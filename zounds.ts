﻿/*! for licence details, see https://gitlab.com/dave.ursa/Zounds */

namespace Zounds {
    /**
     * Root class for Zounds system.
     * Exposes simple access methods and the ability to instantiate more complex features, like:
     * A @see Library is used to hold your sounds, against their URLs & a more friendly id.
     * A @see Zample is a pre-loaded media file that you can play as many times as you like (concurrently).
     * A @see Zinstance is a currently playing Audio file which you can stop or manipulate in other ways.
     */
    export class Zounds implements Library {
        private readonly innerContext: InnerZoundsContext;
        private loading: number = 0;
        private loadedCallback: (success: boolean, issue: string) => void = () => { };

        private sampleDict: { [url: string]: ZampleInner; } = {};
        private sampleLibrary: { [id: string]: ZampleInner; } = {};

        constructor() {
            this.innerContext = new InnerZoundsContext();
        }

        //Utility Functions

        /**
         * just play the audio media found at the URL until the end.
         * If you wish to pre-cache the file before playing or stop the sound mid-playback you need @see load()
         * @param {string} url the url to the audio media to be played.
         */
        playURL(url: string): void {
            this.load(url, "", true, -1);
        }

        //Library Functions

        /**
       * Asynchronously add a new sample to the library and return the Zample representation
       * @param {string} url the url where the audio media can be found
       * @param {string} id an id to give the sound for later retrvial by @see getSampleById leave as "" to extract just the filename minus the extension e.g. "../assets/sounds/wow.mp3"  would extract 'wow'
       * @param {boolean} playWhenLoaded whether to play the sample as soon as it is loaded, note that the @see Zinstance for this is lost due to the asynchronous nature of loading.
       * @param {number} gapBetweenPlays a number of ms that will be left between play actually working, to prevent the sample being played too often. -1 for no limiting.
       * @return the Zample representing the loaded sound ready to be instantiated
       */
        load(url: string, id: string = "", playWhenLoaded: boolean = false, gapBetweenPlays: number = -1): Zample {
            if (id.length == 0) {
                id = url.split('/').pop().split('.')[0];
            }
            if (this.innerContext.error) {
                // There's to be no sound today folks, 
                // just make this look as much like the real thing to the caller and drop out.
                this.pushLoading();
                setTimeout(() => { this.popLoading() }, 500);
                return this.dummyZample;
            }

            //look for the sample in the already loaded list
            let sample: ZampleInner = this.sampleDict[url];
            if (sample !== undefined) {
                // this sample is already a thing, 
                // mark it to play either now or when loaded
                if (playWhenLoaded) {
                    sample.play();
                }
                // if the sample is in the dict it will already be loading, don't double up
                return sample;
            }

            // make sure the system knows there's another sample loading
            this.pushLoading();

            //create the sample object to contain the data
            sample = new ZampleInner(url, playWhenLoaded, gapBetweenPlays, (buf: ZampleInner) => { return this.innerContext.playBuffer(buf); });

            //add the object to the dictionary and the Library
            this.sampleDict[url] = sample;
            this.sampleLibrary[id] = sample;

            //now begin the async loading jazz
            let req: XMLHttpRequest = new XMLHttpRequest();

            req.open('GET', url, true);
            req.responseType = 'arraybuffer';
            req.onload = (ev: Event) => {
                if (req.status == 200) {
                    this.innerContext.context.decodeAudioData(req.response, (buffer: AudioBuffer) => {
                        sample.buffer = buffer;
                        sample.loadComplete = true;
                        if (sample.playWhenLoaded) {
                            sample.play();
                        }
                        this.popLoading();
                    }
                    );
                } else {
                    // HTTP status != 200
                    //deal with the callback
                    this.loadedCallback(false, req.statusText + ": " + url);
                    throw new Error(req.statusText + ": " + url);
                }
            }
            req.onerror = function () {
                throw new Error("Network Error");
            };
            req.send();
            return sample;

        }

        private dummyZinstance = new Zinstance(null, null);
        private dummyZample = new ZampleInner("dummy", false, 0, () => {/*nop */ return this.dummyZinstance; });

        /**
         * Retrieve the Zample that is tied to the give id on this Library.
         * @param {string} id the id as used when the sample was loaded @see load()
         */
        getById(id: string): Zample {
            return this.sampleLibrary[id];
        }

        /**
         * Play the sample that is tied to the give id on this Library.
         * @param {string} id
         */
        playById(id: string): Zinstance {
            let zamp: Zample = this.getById(id);
            return zamp.play();
        }

        public getLibrary(): Library {
            return this;
        }

        private pushLoading(): void {
            this.loading++;
        }

        private popLoading(): void {
            this.loading--;
            if (this.loading == 0) {
                this.loadedCallback(true, "");
                this.loadedCallback = () => { };
            }
        }

        /**
        * Set a callback for sample loading.
        * This will either be called when all samples are loaded.
        * or once for each sample that fails to load.
        * Note, that if more samples are added to be loaded after success, it will not be called again.
        * if loading is already complete this will be called synchronously.
        * @param cback the callback function, the params will indicate the outcome and cause of a failure
        */
        public callBackWhenComplete(cback: (success: boolean, issue: string) => void): void {
            this.loadedCallback = cback;
            if (this.innerContext.context === undefined) {
                cback(false, "Sound not supported.");
            }
            if (this.loading == 0) {
                this.loadedCallback(true, "");
                this.loadedCallback = () => { };
            } else {
                //still loading
                //callback has been registered and will trigger when ready.
            }
        }

        /**
         * A debugging assistance function, used as a last resort to see what didn't load.
         * e.g. from the browser jscript console type: zounds.getSamplesStillLoading()
         * Whilst an empty set could be a way of checking for whether loading is done, you really
         * should use the @see callBackWhenComplete method.
         *
         * @returns the array of sample urls that have not exited the loading state (either failed or waiting)
         */
        public getSamplesStillLoading(): string[] {
            let rval: string[] = [];
            for (let url in this.sampleDict) {
                if (!this.sampleDict[url].loadComplete) {
                    rval[rval.length] = url;
                }
            }
            return rval;
        }
    }

    /**
     * Think of this as a namespace of pre-loaded audio files that can be started as required
     * Normally you only need one of these, but the option is there to have more than one namespace of samples.
     */
    export interface Library {

        /**
         * Asynchronously add a new sample to the library and return the Zample representation
         * @param {string} url the url where the audio media can be found
         * @param {string} id an id to give the sound for later retrvial by @see getSampleById leave as "" to extract just the filename minus the extension e.g. "../assets/sounds/wow.mp3"  would extract 'wow'
         * @param {boolean} playWhenLoaded whether to play the sample as soon as it is loaded, note that the @see Zinstance for this is lost due to the asynchronous nature of loading.
         * @param {number} gapBetweenPlays a number of ms that will be left between play actually working, to prevent the sample being played too often. -1 for no limiting.
         * @return the Zample representing the loaded sound ready to be instantiated
         */
        load(url: string, id: string, playWhenLoaded: boolean, gapBetweenPlays: number): Zample;

        /**
         * Asynchronously add a new sample to the library and return the Zample representation
         * @param {string} url the url where the audio media can be found
         * @return the Zample representing the loaded sound ready to be instantiated
        */
        load(url: string): Zample;

        /**
         * Retrieve the Zample that is tied to the give id on this Library.
         * @param {string} id the id as used when the sample was loaded @see load()
         */
        getById(id: string): Zample;

        /**
         * Play the sample that is tied to the give id on this Library.
         * @param {string} id
         */
        playById(id: string): Zinstance;

        /**
        * Set a callback to track the status of sample loading.
        * This will either be called when all samples are loaded, or once for each sample that fails to load.
        * Note, that if more samples are added to be loaded after success, it will not be called again.
        * if loading is already complete this will be called synchronously.
        * @param cback the callback function, the params will indicate the outcome and cause of a failure
        */
        callBackWhenComplete(cback: (success: boolean, issue: string) => void): void;
    }

    /**
     * a class to wrap the audio context and handle any issues with it.
     */
    class InnerZoundsContext {
        context: AudioContext;
        public error: boolean = true;

        constructor() {
            try {
                //cope with odd issue 
                window["AudioContext"] = window["AudioContext"] || window["webkitAudioContext"];
                this.context = new AudioContext();
                this.error = false;
            }
            catch (e) {
                console.log("No Audio. Exception: " + e);
            }
        }

        playBuffer(samp: ZampleInner): Zinstance {
            if (this.error) {
                throw new Error("Cannot play the buffer as there is no Audio Context");
            }
            let source: AudioBufferSourceNode = this.context.createBufferSource();
            source.loop = samp.loop;
            source.connect(this.context.destination);
            source.buffer = samp.buffer;
            source.start();
            return new Zinstance(source, samp);
        }
    }

    /**
     * represents a pre-loaded audio clip.
     */
    export interface Zample {
        url: string;
        play(): Zinstance;
        loop: boolean;
        setMaxPlayFrequency(ms: number);
    }

    class ZampleInner implements Zample {
        buffer: AudioBuffer;
        loadComplete: boolean = false;
        loop: boolean = false;
        lastPlayed: number = 0;
        constructor(public url: string, public playWhenLoaded: boolean, public maxPlayFreq, private playRaw: (buf: ZampleInner) => Zinstance) {

        }

        play(): Zinstance {
            if (!this.loadComplete) {
                this.playWhenLoaded = true;
                return null;
            }

            if (this.maxPlayFreq > 0) {
                let tslp = Date.now() - this.lastPlayed;
                if (tslp < this.maxPlayFreq) {
                    // don't play, as this has happened too recently.
                    return null;
                } else {
                    this.lastPlayed = Date.now();
                }
            }
            return this.playRaw(this);
        }

        setMaxPlayFrequency(ms: number): void {
            this.maxPlayFreq = ms;
        }
    }

    export class Zinstance {
        constructor(public readonly bsNode: AudioBufferSourceNode, public readonly zample: Zample) {

        }

        stop(): void {
            this.bsNode.stop();
        }
    }
}

/**
 * An instance of the zounds system for the current page.
 */
var zounds = new Zounds.Zounds();